function remote_init()
	local items={
		{name="Keyboard", input="keyboard"},
		{name="Mod Wheel", input="value", min=0, max=127},
		{name="Pitch Bend", input="value", min=0, max=16384},
		{name="Damper Pedal", input="value", min=0, max=127},

		{name="click", input="button"},
		{name="quant", input="button"},
		{name="rew", input="button"},
		{name="fwd", input="button"},
		{name="stop", input="button"},
		{name="play", input="button"},
		{name="record", input="button"},
		{name="loop", input="button"},

		{name="volume", input="delta"},
		{name="cut", input="value", min=0, max=127},
		{name="reso", input="value", min=0, max=127},
		{name="lforate", input="value", min=0, max=127},
		{name="lfoamt", input="value", min=0, max=127},
		{name="chorus", input="value", min=0, max=127},
		{name="atk1", input="value", min=0, max=127},
		{name="dec1", input="value", min=0, max=127},

		{name="preset", input="delta"},
		{name="param1", input="value", min=0, max=127},
		{name="param2", input="value", min=0, max=127},
		{name="param3", input="value", min=0, max=127},
		{name="param4", input="value", min=0, max=127},
		{name="delay", input="value", min=0, max=127},
		{name="sus1", input="value", min=0, max=127},
		{name="rel1", input="value", min=0, max=127},
		
		{name="pushbutton1", input="button"},
		{name="pushbutton2", input="button"},

			}
	remote.define_items(items)

	local inputs={

		{pattern="b0 16 00", name="click", value="1"},
		{pattern="b0 17 00", name="quant", value="1"},

		{pattern="b0 18 xx", name="rew", value="1"},
		{pattern="b0 19 xx", name="fwd", value="1"},
		{pattern="b0 1a 7f", name="stop", value="1"},
		{pattern="b0 1b 7f", name="play", value="1"},
		{pattern="b0 1c 7f", name="record", value="1"},
		{pattern="b0 1d 7f", name="loop", value="1"},

		{pattern="b0 70 xx", name="volume", value="(64 - x) * -1"},
		{pattern="b0 4A xx", name="cut"},
		{pattern="b0 47 xx", name="reso"},
		{pattern="b0 4C xx", name="lforate"},
		{pattern="b0 4D xx", name="lfoamt"},
		{pattern="b0 5D xx", name="chorus"},
		{pattern="b0 49 xx", name="atk1"},
		{pattern="b0 4B xx", name="dec1"},

		{pattern="b0 72 xx", name="preset", value="(64 - x) * -1"},
		{pattern="b0 12 xx", name="param1"},
		{pattern="b0 13 xx", name="param2"},
		{pattern="b0 10 xx", name="param3"},
		{pattern="b0 11 xx", name="param4"},
		{pattern="b0 5B xx", name="delay"},
		{pattern="b0 4F xx", name="sus1"},
		{pattern="b0 48 xx", name="rel1"},

		{pattern="e0 xx yy", name="Pitch Bend", value="y*128 + x"},

		{pattern="b0 01 xx", name="Mod Wheel"},
		{pattern="b0 40 xx", name="Damper Pedal"},

		{pattern="80 xx yy", name="Keyboard", value="0", note="x", velocity="64"},		
		{pattern="90 xx 00", name="Keyboard", value="0", note="x", velocity="64"},
		{pattern="<100x>0 yy zz", name="Keyboard"},

		{pattern="89 xx yy", name="Keyboard", value="0", note="x", velocity="64"},
		{pattern="99 xx 00", name="Keyboard", value="0", note="x", velocity="64"},
		{pattern="<100x>9 yy zz", name="Keyboard"},

		{pattern="b0 71 7f", name="pushbutton1", value="1"},
		{pattern="b0 73 7f", name="pushbutton2", value="1"},
	
	}
	remote.define_auto_inputs(inputs)
end


function remote_probe()

	local controlRequest="F0 7E 7F 06 01 F7"

	local controlResponse="F0 7E 00 06 02 00 20 6B 02 00 04 ?? ?? ?? ?? ?? F7"
return {
		request=controlRequest,
		response=controlResponse,
	}
end

