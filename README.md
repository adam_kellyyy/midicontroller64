This repository contains the .ino file that runs the microcontroller in the midiBox, along with the necessary development files required to enable it to work seamlessly with Reason.

The device has 64 buttons and 64 LEDs in an 8x8 configuration, with 3 additional programmable function buttons.

![midiBox](midiBox.png)