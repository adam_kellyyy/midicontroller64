for i in range(64):
    string = hex(i)
    string = string.replace("0x", "")
    if len(string) == 1:
        string = "0" + string

    print(f'{{name="Button {i}", pattern="01 {string} 0<000x>"}},')