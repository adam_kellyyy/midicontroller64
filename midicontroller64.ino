#include <Bounce.h>
#define BUTTONS 0
#define POTS 1

// working back pins: 29, 30, 31, 33

// activation pins:
// 0, 1, 2, 3, 4, 5, 6, 7

// button pins:
// 8, 9, 10, 11, 12, 13, 14, 15, 16

// led pins:
// 17, 18, 19, 20, 21, 22, 23, 29

// utility buttons:
// 29
// 30
// 31
// 32

// status leds:
// 26
// 27
// 28

// track colours:

int chords[] = {255, 0, 0};
int bass[] = {255, 255, 0};
int drums[] = {255, 100, 0};
int lead[] = {0, 255, 0};
int noise[] = {0, 255, 50};
int percussion[] = {0, 200, 255};
int strings[] = {0, 0, 255};
int vocals[] = {200, 0, 255};

// rgb pot variables:

int potValue = 0;
int rowSelection = 0;
bool potLatch = false;
bool potButtonLatch = false;

int leftMarkerPositions[] = {2, 50, 70, 86, 94, 102};
int rightMarkerPositions[] = {18, 58, 78, 94, 102, 114};

int currentLeft = leftMarkerPositions[0];
int currentRight = rightMarkerPositions[0];

int currentLoop = 0;
int previousLoop = 0;
int numLoops = 6;

const int channel = 1;
int activeOutput = 0;
int physicalButtons[64] = {0};
int buttonLED[64] = {0};
int buttons[64] = {0};
int pots[8] = {0};
int rowMode[8] = {0};
int potLED[64] = {0};
int ledPins[8] = {17, 18, 19, 20, 21, 22, 23, 29};

//-----------------NOT USING ANYMORE-------------------//
// pot pins:
// r -> 21
// g -> 22
// b -> 23
// sw -> 29
// + -> 3.3V
// a -> 30
// b -> 31
// c -> GND

Bounce button0 = Bounce(8, 5);Bounce button1 = Bounce(8, 5);Bounce button2 = Bounce(8, 5);Bounce button3 = Bounce(8, 5);Bounce button4 = Bounce(8, 5);Bounce button5 = Bounce(8, 5);Bounce button6 = Bounce(8, 5);Bounce button7 = Bounce(8, 5);Bounce button8 = Bounce(9, 5);Bounce button9 = Bounce(9, 5);Bounce button10 = Bounce(9, 5);Bounce button11 = Bounce(9, 5);Bounce button12 = Bounce(9, 5);Bounce button13 = Bounce(9, 5);Bounce button14 = Bounce(9, 5);Bounce button15 = Bounce(9, 5);Bounce button16 = Bounce(10, 5);Bounce button17 = Bounce(10, 5);Bounce button18 = Bounce(10, 5);Bounce button19 = Bounce(10, 5);Bounce button20 = Bounce(10, 5);Bounce button21 = Bounce(10, 5);Bounce button22 = Bounce(10, 5);Bounce button23 = Bounce(10, 5);Bounce button24 = Bounce(11, 5);Bounce button25 = Bounce(11, 5);Bounce button26 = Bounce(11, 5);Bounce button27 = Bounce(11, 5);Bounce button28 = Bounce(11, 5);Bounce button29 = Bounce(11, 5);Bounce button30 = Bounce(11, 5);Bounce button31 = Bounce(11, 5);Bounce button32 = Bounce(12, 5);Bounce button33 = Bounce(12, 5);Bounce button34 = Bounce(12, 5);Bounce button35 = Bounce(12, 5);Bounce button36 = Bounce(12, 5);Bounce button37 = Bounce(12, 5);Bounce button38 = Bounce(12, 5);Bounce button39 = Bounce(12, 5);Bounce button40 = Bounce(14, 5);Bounce button41 = Bounce(14, 5);Bounce button42 = Bounce(14, 5);Bounce button43 = Bounce(14, 5);Bounce button44 = Bounce(14, 5);Bounce button45 = Bounce(14, 5);Bounce button46 = Bounce(14, 5);Bounce button47 = Bounce(14, 5);Bounce button48 = Bounce(15, 5);Bounce button49 = Bounce(15, 5);Bounce button50 = Bounce(15, 5);Bounce button51 = Bounce(15, 5);Bounce button52 = Bounce(15, 5);Bounce button53 = Bounce(15, 5);Bounce button54 = Bounce(15, 5);Bounce button55 = Bounce(15, 5);Bounce button56 = Bounce(16, 5);Bounce button57 = Bounce(16, 5);Bounce button58 = Bounce(16, 5);Bounce button59 = Bounce(16, 5);Bounce button60 = Bounce(16, 5);Bounce button61 = Bounce(16, 5);Bounce button62 = Bounce(16, 5);Bounce button63 = Bounce(16, 5);

Bounce utility1 = Bounce(30, 5);
Bounce utility2 = Bounce(31, 5);
Bounce utility3 = Bounce(33, 5);


void setup() {

	for (int i = 0; i < 8; i ++) {
		rowMode[i] = BUTTONS;
	}

	
    usbMIDI.setHandleNoteOn(myNoteOn);
  	usbMIDI.setHandleNoteOff(myNoteOff);
  	usbMIDI.setHandleControlChange(myControlChange);

	// activation pins are set to LOW, so that when a button is pressed the input is pulled down
	// this means that the output pins for the LED's are set to HIGH, and that current flows from the ouput pins to the activation pins
	// the positive (longer leg) of the LED's go into pins 17 - 24

	// output pins for activating a column of buttons and LED's

	pinMode(0, OUTPUT);
	pinMode(1, OUTPUT);
	pinMode(2, OUTPUT);
	pinMode(3, OUTPUT);
	pinMode(4, OUTPUT);
	pinMode(5, OUTPUT);
	pinMode(6, OUTPUT);
	pinMode(7, OUTPUT);
	

	// input pins for each row of buttons

	pinMode(8, INPUT_PULLUP);
	pinMode(9, INPUT_PULLUP);
	pinMode(10, INPUT_PULLUP);
	pinMode(11, INPUT_PULLUP);
	pinMode(12, INPUT_PULLUP);
	pinMode(14, INPUT_PULLUP);
	pinMode(15, INPUT_PULLUP);
	pinMode(16, INPUT_PULLUP);

	// output pins for turning on LED's

	pinMode(17, OUTPUT);
	pinMode(18, OUTPUT);
	pinMode(19, OUTPUT);
	pinMode(20, OUTPUT);
	pinMode(21, OUTPUT);
	pinMode(22, OUTPUT);
	pinMode(23, OUTPUT);
	pinMode(29, OUTPUT);

	// pins for utility buttons

	pinMode(30, INPUT_PULLUP);
	pinMode(31, INPUT_PULLUP);
	pinMode(33, INPUT_PULLUP);




}

void loop() {


	buttonsToLEDS();
	potsToLEDS();
	// updateRGBPot();

	usbMIDI.read();
	activeOutput++;
	if (activeOutput > 7) {
		activeOutput = 0;
	}

	scanButtons();
	usbMIDI.read();

	// increments loop marker positions, goes back to the start if the end is reached

	utility1.update();

	if (utility1.fallingEdge()) {
		resetLoop();
    	previousLoop = currentLoop;
		currentLoop --;
		if (currentLoop < 0) {
			currentLoop = numLoops-1;
			setLoop(leftMarkerPositions[currentLoop], rightMarkerPositions[currentLoop]);
		} else {
			loopChange(true);
		}		

	}

	utility2.update();

	if (utility2.fallingEdge()) {
		resetLoop();
    	previousLoop = currentLoop;
		currentLoop ++;
		if (currentLoop > numLoops - 1) {
			currentLoop = 0;
			loopChange(true);
		} else {
			setLoop(leftMarkerPositions[currentLoop], rightMarkerPositions[currentLoop]);
		}

	}

	utility3.update();

	if (utility3.fallingEdge()) {
	}
	
}

void loopChange(bool left) {
	if (left) {
		if (currentLoop == 0) {
      setLoop(leftMarkerPositions[currentLoop], rightMarkerPositions[previousLoop]);
		} else {
		  setLoop(leftMarkerPositions[currentLoop], rightMarkerPositions[previousLoop]);
		}
	} else {
		resetLoop();
		setLoop(leftMarkerPositions[currentLoop], rightMarkerPositions[currentLoop]);
	}
}

void setLoop(int left, int right) {
	for (int i = 1; i < left; i ++) {
		usbMIDI.sendControlChange(0x19,1, channel);
		delay(0.2);
		currentLeft ++;
		
	}

	for (int i = 1; i < right; i ++) {
		usbMIDI.sendControlChange(0x20,1, channel);
		delay(0.2);
		currentRight ++;
	}

}

void resetLoop() {

	if (currentRight > currentLeft) {
		while(currentLeft != currentRight) {
			usbMIDI.sendControlChange(0x19,1, channel);
			delay(0.2);
			currentLeft++;
		}
	} else {
		while(currentLeft != currentRight) {
			usbMIDI.sendControlChange(0x20,1, channel);
			delay(0.2);
			currentRight++;
		}
	}

	while(currentLeft != 1) {
		usbMIDI.sendNoteOn(0x40, 127, channel);
		delay(0.2);
		currentLeft--;
		currentRight--;
	}

}

void buttonsToLEDS() {
	for (int i = 0; i < 8; i ++) {
		for (int j = 0; j < 8; j ++) {
			buttonLED[(j*8)+i] = buttons[(i*8)+j];
		}
	}
}

void potsToLEDS() {
	for (int i = 0; i < 8; i ++) {
		for (int j = 0; j < 8; j ++) {
			potLED[(i*8)+j] = 0;
		}
	}

	for (int i = 0; i < 8; i ++) {
		if (pots[i] != 0) {
			int temp = pots[i]-1;
			potLED[((temp/17)*8)+i] = 1;
		}
	}
}


void noteOn (int note) {
	if (rowMode[note/8] == BUTTONS) {
		usbMIDI.sendNoteOn(note, 127, channel);
	} else {
		usbMIDI.sendControlChange(17+(note%8),(note/8)*17, channel);
	}
}

void setOutput (int pin) {
	for (int i = 0; i < 8; i ++) {
		if (i != pin) {
			digitalWrite(i, HIGH);
		}
	}
	if (pin != -1) {
		digitalWrite(pin, LOW);
	}
}

void updateLEDS() {

	for (int i = 0; i < 8; i ++) {
		digitalWrite(ledPins[i], LOW);
	}

    for (int i = 0; i < 8; i ++) {
        if (rowMode[activeOutput] == BUTTONS) {
            if (buttonLED[i+(activeOutput*8)]) {
				digitalWrite(ledPins[i], HIGH);
				Serial.println(ledPins[i]);
				Serial.println(activeOutput);
			}
        } else {
			if (potLED[activeOutput+(i*8)]) {
				digitalWrite(ledPins[i], HIGH);
			}
        }
    }
}

void myNoteOn(byte chan, byte note, byte velocity) {
	if (chan == channel && note >= 0 && note < 64) {
		buttons[note] = velocity;
	}

	if (chan == channel && note == 127) {
		loopChange(false);
	}
	
}

void myNoteOff(byte chan, byte note, byte velocity) {
	if (chan == channel && note >= 0 && note < 64) {
		buttons[note] = velocity;
	}
}

void myControlChange(byte chan, byte control, byte value) {
	// Serial.print(chan);
	// Serial.print(" ");
	// Serial.print(control);
	// Serial.print(" ");
	// Serial.println(value);
	// if (chan == channel && control > 16 && control < 25) {
	// 	pots[control-17] = value;
	// }
	if (chan == channel && control >= 0 && control < 64) {
		buttons[control] = value;
	}
}

void updateRGBPot() {
	if (digitalRead(31) && !digitalRead(30) && !potLatch) {
		potValue++;
		if (potValue > 1023) {
			potValue = 0;
		}
		potLatch = true;
	} else if (!digitalRead(31) && digitalRead(30) && !potLatch) {
		potValue--;
		if (potValue < 0) {
			potValue = 1023;
		}
		potLatch = true;
	} else if (digitalRead(31) && digitalRead(30)) {
		potLatch = false;
	}

	rowSelection = (potValue/32)%8;
	// Serial.println(digitalRead(29));
	digitalWrite(29, LOW);
	if (digitalRead(29) && !potButtonLatch) {
		potButtonLatch = true;
		changeMode(rowSelection);
	} else if (!digitalRead(29) && potButtonLatch) {
		potButtonLatch = false;
	}
	switch (rowSelection) {
		case 0:
			setRGB(chords);
			break;
		case 1:
			setRGB(bass);
			break;
		case 2:
			setRGB(drums);
			break;
		case 3:
			setRGB(lead);
			break;
		case 4:
			setRGB(noise);
			break;
		case 5:
			setRGB(percussion);
			break;
		case 6:
			setRGB(strings);
			break;
		case 7:
			setRGB(vocals);
			break;
	}
        
}

void setRGB(int rgb[]) {
	analogWrite(21, 255-rgb[0]);
	analogWrite(22, 255-rgb[1]);
	analogWrite(23, 255-rgb[2]);
}

void changeMode (int rowSelection) {
	if (rowMode[rowSelection] == BUTTONS) {
		rowMode[rowSelection] = POTS;
	} else {
		rowMode[rowSelection] = BUTTONS;
	}
}

void scanButtons() {
	delay(1);
	switch (activeOutput) {

		case 0:
			setOutput(0);

			updateLEDS();

			button0.update();

			if (button0.fallingEdge()) {
				noteOn(0);
				physicalButtons[0] = 1;
			}

			if (button0.risingEdge()) {
				physicalButtons[0] = 0;
			}

			button8.update();

			if (button8.fallingEdge()) {
				noteOn(8);
				physicalButtons[1] = 1;
			}

			if (button8.risingEdge()) {
				physicalButtons[1] = 0;
			}

			button16.update();

			if (button16.fallingEdge()) {
				noteOn(16);
				physicalButtons[2] = 1;
			}

			if (button16.risingEdge()) {
				physicalButtons[2] = 0;
			}

			button24.update();

			if (button24.fallingEdge()) {
				noteOn(24);
				physicalButtons[3] = 1;
			}

			if (button24.risingEdge()) {
				physicalButtons[3] = 0;
			}

			button32.update();

			if (button32.fallingEdge()) {
				noteOn(32);
				physicalButtons[4] = 1;
			}

			if (button32.risingEdge()) {
				physicalButtons[4] = 0;
			}

			button40.update();

			if (button40.fallingEdge()) {
				noteOn(40);
				physicalButtons[5] = 1;
			}

			if (button40.risingEdge()) {
				physicalButtons[5] = 0;
			}

			button48.update();

			if (button48.fallingEdge()) {
				noteOn(48);
				physicalButtons[6] = 1;
			}

			if (button48.risingEdge()) {
				physicalButtons[6] = 0;
			}

			button56.update();

			if (button56.fallingEdge()) {
				noteOn(56);
				physicalButtons[7] = 1;
			}

			if (button56.risingEdge()) {
				physicalButtons[7] = 0;
			}

			break;

		case 1:
			setOutput(1);

			updateLEDS();

			button1.update();

			if (button1.fallingEdge()) {
				noteOn(1);
				physicalButtons[8] = 1;
			}

			if (button1.risingEdge()) {
				physicalButtons[8] = 0;
			}

			button9.update();

			if (button9.fallingEdge()) {
				noteOn(9);
				physicalButtons[9] = 1;
			}

			if (button9.risingEdge()) {
				physicalButtons[9] = 0;
			}

			button17.update();

			if (button17.fallingEdge()) {
				noteOn(17);
				physicalButtons[10] = 1;
			}

			if (button17.risingEdge()) {
				physicalButtons[10] = 0;
			}

			button25.update();

			if (button25.fallingEdge()) {
				noteOn(25);
				physicalButtons[11] = 1;
			}

			if (button25.risingEdge()) {
				physicalButtons[11] = 0;
			}

			button33.update();

			if (button33.fallingEdge()) {
				noteOn(33);
				physicalButtons[12] = 1;
			}

			if (button33.risingEdge()) {
				physicalButtons[12] = 0;
			}

			button41.update();

			if (button41.fallingEdge()) {
				noteOn(41);
				physicalButtons[13] = 1;
			}

			if (button41.risingEdge()) {
				physicalButtons[13] = 0;
			}

			button49.update();

			if (button49.fallingEdge()) {
				noteOn(49);
				physicalButtons[14] = 1;
			}

			if (button49.risingEdge()) {
				physicalButtons[14] = 0;
			}

			button57.update();

			if (button57.fallingEdge()) {
				noteOn(57);
				physicalButtons[15] = 1;
			}

			if (button57.risingEdge()) {
				physicalButtons[15] = 0;
			}

			break;

		case 2:
			setOutput(2);

			updateLEDS();

			button2.update();

			if (button2.fallingEdge()) {
				noteOn(2);
				physicalButtons[16] = 1;
			}

			if (button2.risingEdge()) {
				physicalButtons[16] = 0;
			}

			button10.update();

			if (button10.fallingEdge()) {
				noteOn(10);
				physicalButtons[17] = 1;
			}

			if (button10.risingEdge()) {
				physicalButtons[17] = 0;
			}

			button18.update();

			if (button18.fallingEdge()) {
				noteOn(18);
				physicalButtons[18] = 1;
			}

			if (button18.risingEdge()) {
				physicalButtons[18] = 0;
			}

			button26.update();

			if (button26.fallingEdge()) {
				noteOn(26);
				physicalButtons[19] = 1;
			}

			if (button26.risingEdge()) {
				physicalButtons[19] = 0;
			}

			button34.update();

			if (button34.fallingEdge()) {
				noteOn(34);
				physicalButtons[20] = 1;
			}

			if (button34.risingEdge()) {
				physicalButtons[20] = 0;
			}

			button42.update();

			if (button42.fallingEdge()) {
				noteOn(42);
				physicalButtons[21] = 1;
			}

			if (button42.risingEdge()) {
				physicalButtons[21] = 0;
			}

			button50.update();

			if (button50.fallingEdge()) {
				noteOn(50);
				physicalButtons[22] = 1;
			}

			if (button50.risingEdge()) {
				physicalButtons[22] = 0;
			}

			button58.update();

			if (button58.fallingEdge()) {
				noteOn(58);
				physicalButtons[23] = 1;
			}

			if (button58.risingEdge()) {
				physicalButtons[23] = 0;
			}

			break;

		case 3:
			setOutput(3);

			updateLEDS();

			button3.update();

			if (button3.fallingEdge()) {
				noteOn(3);
				physicalButtons[24] = 1;
			}

			if (button3.risingEdge()) {
				physicalButtons[24] = 0;
			}

			button11.update();

			if (button11.fallingEdge()) {
				noteOn(11);
				physicalButtons[25] = 1;
			}

			if (button11.risingEdge()) {
				physicalButtons[25] = 0;
			}

			button19.update();

			if (button19.fallingEdge()) {
				noteOn(19);
				physicalButtons[26] = 1;
			}

			if (button19.risingEdge()) {
				physicalButtons[26] = 0;
			}

			button27.update();

			if (button27.fallingEdge()) {
				noteOn(27);
				physicalButtons[27] = 1;
			}

			if (button27.risingEdge()) {
				physicalButtons[27] = 0;
			}

			button35.update();

			if (button35.fallingEdge()) {
				noteOn(35);
				physicalButtons[28] = 1;
			}

			if (button35.risingEdge()) {
				physicalButtons[28] = 0;
			}

			button43.update();

			if (button43.fallingEdge()) {
				noteOn(43);
				physicalButtons[29] = 1;
			}

			if (button43.risingEdge()) {
				physicalButtons[29] = 0;
			}

			button51.update();

			if (button51.fallingEdge()) {
				noteOn(51);
				physicalButtons[30] = 1;
			}

			if (button51.risingEdge()) {
				physicalButtons[30] = 0;
			}

			button59.update();

			if (button59.fallingEdge()) {
				noteOn(59);
				physicalButtons[31] = 1;
			}

			if (button59.risingEdge()) {
				physicalButtons[31] = 0;
			}

			break;

		case 4:
			setOutput(4);

			updateLEDS();

			button4.update();

			if (button4.fallingEdge()) {
				noteOn(4);
				physicalButtons[32] = 1;
			}

			if (button4.risingEdge()) {
				physicalButtons[32] = 0;
			}

			button12.update();

			if (button12.fallingEdge()) {
				noteOn(12);
				physicalButtons[33] = 1;
			}

			if (button12.risingEdge()) {
				physicalButtons[33] = 0;
			}

			button20.update();

			if (button20.fallingEdge()) {
				noteOn(20);
				physicalButtons[34] = 1;
			}

			if (button20.risingEdge()) {
				physicalButtons[34] = 0;
			}

			button28.update();

			if (button28.fallingEdge()) {
				noteOn(28);
				physicalButtons[35] = 1;
			}

			if (button28.risingEdge()) {
				physicalButtons[35] = 0;
			}

			button36.update();

			if (button36.fallingEdge()) {
				noteOn(36);
				physicalButtons[36] = 1;
			}

			if (button36.risingEdge()) {
				physicalButtons[36] = 0;
			}

			button44.update();

			if (button44.fallingEdge()) {
				noteOn(44);
				physicalButtons[37] = 1;
			}

			if (button44.risingEdge()) {
				physicalButtons[37] = 0;
			}

			button52.update();

			if (button52.fallingEdge()) {
				noteOn(52);
				physicalButtons[38] = 1;
			}

			if (button52.risingEdge()) {
				physicalButtons[38] = 0;
			}

			button60.update();

			if (button60.fallingEdge()) {
				noteOn(60);
				physicalButtons[39] = 1;
			}

			if (button60.risingEdge()) {
				physicalButtons[39] = 0;
			}

			break;

		case 5:
			setOutput(5);

			updateLEDS();

			button5.update();

			if (button5.fallingEdge()) {
				noteOn(5);
				physicalButtons[40] = 1;
			}

			if (button5.risingEdge()) {
				physicalButtons[40] = 0;
			}

			button13.update();

			if (button13.fallingEdge()) {
				noteOn(13);
				physicalButtons[41] = 1;
			}

			if (button13.risingEdge()) {
				physicalButtons[41] = 0;
			}

			button21.update();

			if (button21.fallingEdge()) {
				noteOn(21);
				physicalButtons[42] = 1;
			}

			if (button21.risingEdge()) {
				physicalButtons[42] = 0;
			}

			button29.update();

			if (button29.fallingEdge()) {
				noteOn(29);
				physicalButtons[43] = 1;
			}

			if (button29.risingEdge()) {
				physicalButtons[43] = 0;
			}

			button37.update();

			if (button37.fallingEdge()) {
				noteOn(37);
				physicalButtons[44] = 1;
			}

			if (button37.risingEdge()) {
				physicalButtons[44] = 0;
			}

			button45.update();

			if (button45.fallingEdge()) {
				noteOn(45);
				physicalButtons[45] = 1;
			}

			if (button45.risingEdge()) {
				physicalButtons[45] = 0;
			}

			button53.update();

			if (button53.fallingEdge()) {
				noteOn(53);
				physicalButtons[46] = 1;
			}

			if (button53.risingEdge()) {
				physicalButtons[46] = 0;
			}

			button61.update();

			if (button61.fallingEdge()) {
				noteOn(61);
				physicalButtons[47] = 1;
			}

			if (button61.risingEdge()) {
				physicalButtons[47] = 0;
			}

			break;

		case 6:
			setOutput(6);

			updateLEDS();

			button6.update();

			if (button6.fallingEdge()) {
				noteOn(6);
				physicalButtons[48] = 1;
			}

			if (button6.risingEdge()) {
				physicalButtons[48] = 0;
			}

			button14.update();

			if (button14.fallingEdge()) {
				noteOn(14);
				physicalButtons[49] = 1;
			}

			if (button14.risingEdge()) {
				physicalButtons[49] = 0;
			}

			button22.update();

			if (button22.fallingEdge()) {
				noteOn(22);
				physicalButtons[50] = 1;
			}

			if (button22.risingEdge()) {
				physicalButtons[50] = 0;
			}

			button30.update();

			if (button30.fallingEdge()) {
				noteOn(30);
				physicalButtons[51] = 1;
			}

			if (button30.risingEdge()) {
				physicalButtons[51] = 0;
			}

			button38.update();

			if (button38.fallingEdge()) {
				noteOn(38);
				physicalButtons[52] = 1;
			}

			if (button38.risingEdge()) {
				physicalButtons[52] = 0;
			}

			button46.update();

			if (button46.fallingEdge()) {
				noteOn(46);
				physicalButtons[53] = 1;
			}

			if (button46.risingEdge()) {
				physicalButtons[53] = 0;
			}

			button54.update();

			if (button54.fallingEdge()) {
				noteOn(54);
				physicalButtons[54] = 1;
			}

			if (button54.risingEdge()) {
				physicalButtons[54] = 0;
			}

			button62.update();

			if (button62.fallingEdge()) {
				noteOn(62);
				physicalButtons[55] = 1;
			}

			if (button62.risingEdge()) {
				physicalButtons[55] = 0;
			}

			break;

		case 7:
			setOutput(7);

			updateLEDS();

			button7.update();

			if (button7.fallingEdge()) {
				noteOn(7);
				physicalButtons[56] = 1;
			}

			if (button7.risingEdge()) {
				physicalButtons[56] = 0;
			}

			button15.update();

			if (button15.fallingEdge()) {
				noteOn(15);
				physicalButtons[57] = 1;
			}

			if (button15.risingEdge()) {
				physicalButtons[57] = 0;
			}

			button23.update();

			if (button23.fallingEdge()) {
				noteOn(23);
				physicalButtons[58] = 1;
			}

			if (button23.risingEdge()) {
				physicalButtons[58] = 0;
			}

			button31.update();

			if (button31.fallingEdge()) {
				noteOn(31);
				physicalButtons[59] = 1;
			}

			if (button31.risingEdge()) {
				physicalButtons[59] = 0;
			}

			button39.update();

			if (button39.fallingEdge()) {
				noteOn(39);
				physicalButtons[60] = 1;
			}

			if (button39.risingEdge()) {
				physicalButtons[60] = 0;
			}

			button47.update();

			if (button47.fallingEdge()) {
				noteOn(47);
				physicalButtons[61] = 1;
			}

			if (button47.risingEdge()) {
				physicalButtons[61] = 0;
			}

			button55.update();

			if (button55.fallingEdge()) {
				noteOn(55);
				physicalButtons[62] = 1;
			}

			if (button55.risingEdge()) {
				physicalButtons[62] = 0;
			}

			button63.update();

			if (button63.fallingEdge()) {
				noteOn(63);
				physicalButtons[63] = 1;
			}

			if (button63.risingEdge()) {
				physicalButtons[63] = 0;
			}

			break;
	}
}
